Example of customizing postgres for GitLab CI
===================

The purpose of this repository is to show how to use GitLab to do
Continuous Integration with a postgres database.

In order to run this project just fork it on GitLab.com.
Every push will then trigger a new build on GitLab.


The project .gitlab-ci.yaml have been extended: 
- print_job - added to the pipeline
- echo a number of environment variables that are defined, initialised and and maintained by the GitLab CI execution envionment
- add a number of local environments for the project to test how variables can be set for the project environment

---

This is a companion project of <http://doc.gitlab.com/ce/ci/services/postgres.html>.
